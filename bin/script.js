#!/usr/bin/env node

const
  path = require('path')
  fs = require('fs'),
  { parse } = require('node-html-parser'),

  args = process.argv.slice(2)

require('colors')


class Manager {
  constructor() {
    this._list = {}
  }

  load(pathToImagesFolder) {
    console.log('\nLoad static from', pathToImagesFolder.cyan)

    fs.readdirSync(pathToImagesFolder).forEach(filename => {
      const
        fullPath = path.join(pathToImagesFolder, filename),
        ext = path.parse(fullPath).ext
      
      if(ext == '.svg') {
        const fl = new SvgFile(fullPath, filename)
        this._list[fl.filename] = fl.toJSON()
      }
    });
  }

  save(buildPath, buildName) {
    const layout = `const list = ${JSON.stringify(this._list)};export default list;`
    fs.writeFileSync(path.join(buildPath, buildName), layout)
    console.log('\n DONE '.bgBrightGreen.black, `Compiled successfully (${Object.keys(this._list).length}) files\n`.green)
  }
}


class SvgFile {
  constructor(pathToImage, fullName) {
    this.filename = path.parse(pathToImage).name
    this.fullName = fullName

    this.svgText = fs.readFileSync(pathToImage, 'utf8')
  }

  prepareElement() {
    let parseText = parse(this.svgText)
    let svgEl = parseText.tagName != 'svg' ? parseText.querySelector('svg') : parseText

    if(!svgEl)
      throw new Error(`(${this.filename}) File must has svg element!`.red)

    return {
      viewBox: svgEl.getAttribute('viewBox') || '',
      width: svgEl.getAttribute('width') || '1em',
      height: svgEl.getAttribute('height') || '1em',
      path: svgEl.innerHTML
    }
  }

  toJSON() {
    const json = this.prepareElement()
    console.log(`\t\t - ${this.fullName}`.cyan)

    return json
  }
}


if(!args[0])
  throw new Error(`Enter source path!`.red)

if(!args[1])
  throw new Error(`Enter build path!`.red)

const
  sourcePath = path.isAbsolute(args[0]) ? args[0] : path.join(process.cwd(), args[0] || './'),
  buildPath = path.isAbsolute(args[1]) ? args[1] : path.join(process.cwd(), args[1] || './'),
  buildName = args[2] || 'build.js'

const manager = new Manager()
manager.load(sourcePath)
manager.save(buildPath, buildName)