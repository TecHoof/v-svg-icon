/**
 * Lavender magic
 */

import VueSvg from './src/VueSvg.vue'

class Plugin {
  constructor(list, params) {
    this._list = list
    this.defaultWidth = params.defaultWidth || '1em'
    this.defaultHeight = params.defaultHeight || '1em'
    this.class = params.class || ['v-svg-icon']
  }

  getIcon(name) {
    return this._list[name]
  }
}

export default {
  install(Vue, list, params={}) {
    const pl = new Plugin(list, params)
    Vue.component('vue-svg', VueSvg)

    Vue.mixin({
      beforeCreate: function () {
        if(this === this.$root) {
          Vue.util.defineReactive(this, '$vSvgIcons', pl)
        }
      }
    })
  }
};