const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  publicPath: '/',
  runtimeCompiler: true,

  chainWebpack: config => {
    config.resolve.alias
      .set('v-svg-icon', resolve('../'))

    config.resolve.symlinks = false;
  },
}
