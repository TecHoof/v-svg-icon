import Vue from 'vue'
import App from './App.vue'

import listIcons from './assets/icons/build'
import iconPlugin from 'v-svg-icon'

Vue.config.productionTip = false
Vue.use(iconPlugin, listIcons)

new Vue({
  render: h => h(App),
}).$mount('#app')
